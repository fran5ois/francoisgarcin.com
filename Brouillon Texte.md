












//30/10/2018 fichier désué
//Mises à jours sur content.js directement
























# Le Clin: un sac à dos robuste, efficient et bien urbain

Conçu pour une utilisation quotidienne en vélotaf et en transports en commun.

4 ans de recherche, de conception et de prototypage. 12 innovations techniques libres de droit.

Fait-main et sur-mesure à Nice à partir de matériaux de grande qualité fabriqués en France.

De la méthodologie de conception au système d'ouverture en passant par le modèle économique, tout est pensé pour donner au Clin une âme saine, capable de porter sans faille votre charge et un message solide.


[//]: # Description

## Efficient adj. qui produit un maximum de résultats avec un minimum d’efforts.

Le Clin est avant tout un système d'ouverture inédit sans zip ni scratch ni aimant. Il faut acquérir le bon geste pour  ouvrir un Clin, et peut-être même une certaine dextérité. Fastidieux ? - à vous de voir. A ma connaissance il n’existe aucun autre sac à dos que le Clin qui puisse s’ouvrir d’une seule main en 1 seconde et demie... et seulement par son porteur.

Les justées, le système de réglage des suspentes du Clin, sont fabriquées à partir de chaînes de vélo récupérée et offrent une ergonomie inédite. Le réglage instantané et intuitif des justées transforme l'utilisation du sac à dos: en une fraction de seconde, desserrez complètement une bretelle pour mettre et enlever le Clin, en une fraction de seconde, ajustez le Clin au plus près avant un sprint ou avant d'enfourcher un vélo.

Le volume extérieur du Clin s'adapte automatiquement à son contenu, organisé dans 6 poches à usages spécifiques: un "vide-poche" en matière ultra résistante pour les pièces et les clefs, une poche protégée en laine et satin pour accueillir un ordinateur portable ou une tablette, une poche en satin pour accueillir un smartphone, une poche pour porte-document, la grande poche au volume adaptatif et ...une poche pour le reste.

## Robuste adj. capable de résister à des efforts extrêmes et à un usage prolongé

 Les suspentes en Dyneema du Clin supportent 195kg pour 1mm de diamètre et possèdent surtout une résistance à l'abrasion et une durabilité hors du commun.

 L'enveloppe du Clin est confectionnée dans une bâche en lin tissée à l'origine pour un usage militaire. Elle est ultra résistante et son tissage est si dense qu'elle est pratiquement étanche.

 La laine cardée remplace ici les habituelles mousses synthétiques pour une durabilité accrue.

 Les boucles et rivets du Clin sont tous en acier inoxydable.

 En plus des matériaux choisis, c'est la qualité des assemblages du Clin et la conception de sa structure qui font sa robustesse.

 Le Clin est conçu pour vous épauler au quotidien et pour longtemps.


## urbain adj. litt. Qui fait preuve de savoir vivre, de politesse, qui agit avec raffinement.

 Voici l’épaulière. Une surface de contact avec vos épaules la plus large possible, sans pli ni couture, pour répartir parfaitement la pression jusqu’à la rendre imperceptible ; découpée dans le seul matériau aussi élastique et respirant que la peau : … la peau. 

 Aussi discrètes que résistantes, les suspentes du Clin transmettent la charge sans gêner vos mouvements. Elles sont ancrées de telle façon qu’elle ne vous touchent pas et offrent une telle stabilité que la ceinture abdominale, et ses frottements parasites ont disparus.

 Le Clin sait se tenir. Son armature en osier est une combinaison rêvée d’élasticité, de résilience et d’amortissement des chocs. L’architecture du Clin maintien en permanence son contenu au plus près de votre dos et le support lombaire en laine cardée et osier assure un contact confortable, franc et aéré.
 
 Le Clin est rempli de bienveillance. Il est fabriqué en France, avec des matériaux renouvelables (Lin de France, osier de France, cuir Bio d'Allemagne), matériaux réutilisés (chaînes de vélo) ou à très grande durabilité (Inox de France, Dyneema). Le cuir n'est utilisé que là où ses caractéristiques physiques sont irremplaçables. J'ai fabriqué la plupart de mes outils à partir de matériel récupéré.
 
# Démarche
 
 Mes motivations principales sont le plaisir de créer librement, un quotidien équilibré entre travail manuel et intellectuel, l'exercice d'un métier sain et la contribution au renouveau des modes de production et de consommation pour un humain en harmonie avec sa planète.
 
 En choisissant l'entreprise individuelle, je souhaite établir mon outil de travail dans un principe d'économie coopérative et privilégier les relations de partenariat plutôt que les rapports de subordination. Je rêve que la réputation d’excellence du Clin m’amène à collaborer avec d’autres artisans français pour développer d’autres objets du quotidien innovants, durables et produits en France.
 
 La relocalisation de l'artisanat et de la petite industrie est un sujet qui me passionne depuis mes études à l’école nationale d’Arts et Métiers. C’est à mon ambition d'y participer que je dois ma persévérance. Les clés du succès dont je souhaite faire la promotion sont l’artisanat, l'économie coopérative, l'économie circulaire, et des principes de conception et de production innovants, comme la Permafacture.
 
 Au fil de mes expériences, la santé de notre planète est venue se loger au cœur de ma vocation d'ingénieur. Durant la genèse du Clin, l'écoconception a cessé d'être une contrainte à intégrer à mes inventions pour devenir la méthodologie par laquelle je crée. Les 12 principes de permaculture selon David Holmgren ont souvent orienté mes décisions de conception et je souhaite vivement participer à l’avènement de la Permafacture.

 Les outils de production que j'ai créés pour le Clin, notamment ma presse à plisser, sont essentiellement fabriqués à partir de matériaux et machines récupérés. J’ai ainsi, pour un investissement minimal, un outil de production exclusif, performant et adapté à mon produit. En outre, les transformations intervenant dans la confection du Clin n’impliquent aucun outil ou produit dangereux et sont conçues pour ne générer ni déchet, ni poussière, ni émanation gazeuse.
 
 Le Clin a été conçu de façon à être réparable et démontable facilement afin de pouvoir revaloriser ses composants. Ce point pourrait paraître superflu compte tenu de la robustesse du Clin, mais il m’a permis de revaloriser les nombreux prototypes fabriqués et ainsi atteindre mon objectif de zéro déchet.

# Je veux un Clin

 Le Clin commence son aventure en parcourant les grandes villes d’Europe. Après son lancement à Nice cet été, il sera présent au Salon Made in France à Paris les 10-11-12 novembre 2018.

 Actuellement le stock est limité. Lors du Salon MIF les premiers repartiront avec un Clin ajusté sur place à leur mesure, les autres pourront prendre leurs mesures et passer commande pour une livraison début 2019.

 Si vous habitez autour de Nice, contactez moi ou entrez votre numéro ou e-mail:_____

 Vous souhaitez être informer lorsque le Clin fera sont entrée dans votre ville? Entrez votre e-mail et le nom de la grande ville proche de chez vous. ________

 Le Clin est vendu 180€. Un acompte de 90€ est demandé pour chaque commande.

# Liens

 Pour en savoir plus:

 Reportage France 3 Côte d'Azur

 Dossier de presse sur mifexpo.fr

# Contact

contact@francoisgarcin.com

+33(0)659437940

__________

Le Clin fait l'objet d'un dépôt de modèle international qui établi mes droits en tant que créateur et le protège d'une bête "usurpation d'identité". En revanche j’ai fait le choix de ne pas déposer de brevet sur les 12 innovations techniques apportées par le Clin. Je libère ainsi ma création selon les termes de la licence libre « Creative Common, Attribution ». Cela signifie que tout le monde peut réutiliser les solutions techniques du Clin, s’en inspirer librement, ou produire sa propre version du Clin, même pour une utilisation commerciale, à la simple condition de mentionner mon travail. J’invite bien sûr toute personne intéressée à me contacter... librement.

Hébergé par lautre.net, hébergeur associatif autogéré.