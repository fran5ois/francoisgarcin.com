<?php

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        // $name = strip_tags(trim($_POST["name"]));
		// 		$name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        // $message = trim($_POST["message"]);

        // Check that data was sent to the mailer.
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // commande complète : (empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL))
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "francois.garcin@gmail.com";

        // Set the email subject.
        // $subject = "New contact from $name";
        $subject = "Inscription Newsletter LE CLIN";

        // Build the email content.
        // $email_content = "Name: $name\n";
        $email_content = "Email: $email";
        // $email_content .= "Message:\n$message\n";

        // Build the email headers.
        $email_headers = "From: <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            //echo "Thank You! Your message has been sent.";
			send_response($email);
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        // echo "There was a problem with your submission, please try again.";
    }

	function send_response($mail){
		// Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient_response = $mail;

        // Set the email subject.
        $subject_response = "Inscription";
		
        // Build the email response content.       
        $email_content_response = "Merci pour votre inscription à la Newsletter du Clin";
		$email_content_response = "
		<html>
		<head></head>
		<body>
        <h1>Merci pour votre Inscription.</h1></br>
        N'hésitez pas à m'envoyer <a href=\"mailto:francois.garcin@gmail.com\">un mail</a>si vous souhaitez avoir plus d'informations sur le Clin.</br></br>
        François Garcin, Artisan Ingénieur, Inventeur du CLIN.
        <a href=\"http://www.francoisgarcin.com\">www.francoisgarcin.com</a>.
		</body>
		</html>";

        // Build the email headers.
        $email_headers_response = "From: <francois.garcin@gmail.com>\r\n";
		$email_headers_response .= "Content-type: text/html; charset=utf-8\r\n";
		
		if(mail($recipient_response, $subject_response, $email_content_response, $email_headers_response)) {
			http_response_code(200);
			echo "</br>Mail sent.</br>";
		} else
			echo "</br>Error.</br>";
		die('<meta http-equiv = "refresh" content ="2; ../../index.html">');
	}
?>