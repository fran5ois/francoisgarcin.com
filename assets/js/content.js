// -----------------------------------
// MULTI LANGUAGE TEXT CONTENT
// -----------------------------------
const textContent = {
    m_home: {
        fr: `<div class="logo">
        <img src="./assets/images/logo F5 seul.png" alt="Logo" srcset="">
        </div>`,
        en: `<div class="logo">
        <img src="./assets/images/logo F5 seul.png" alt="Logo" srcset="">
        </div>`
    },
    m_clin: {
        fr: `<div class="logo">
        <img src="./assets/images/le Clin lettres.png" alt="Logo" srcset="">
        </div>`,
        en: `<div class="logo">
        <img src="./assets/images/le Clin lettres.png" alt="Logo" srcset="">
        </div>`
    },
    m_demarche: {
        fr: `démarche`,
        en: `philosophy`
    },
    m_order: {
        fr: `commander`,
        en: `order`
    },
    m_liens: {
        fr: `liens`,
        en: `links`
    },
    m_contact: {
        fr: `contact`,
        en: `contact`
    },
    title_clin: {
        fr: `le Clin: un sac à dos robuste, efficient et bien urbain`,
        en: `the Clin: robust, efficient and well urbane`
    },
    p_clin: {
        fr: `Conçu pour une utilisation quotidienne en <strong>vélotaf</strong> et en <strong>transports en commun</strong>.<br/> 
        <br/>
        4 ans de recherche, de conception et de prototypage. 12 <strong>innovations techniques</strong> libres de droit.<br/>
        <br/>
        <strong>Fait-main</strong> et <strong>sur-mesure</strong> à Nice à partir de matériaux de grande qualité fabriqués en France.<br/>
        <br/>
        Tout est pensé pour donner au Clin une âme saine : de la méthodologie d'<strong>écoconception</strong> au système d'ouverture en passant par le modèle économique. Le Clin portera sans faille votre charge ainsi qu'un message à la fois <strong>écologique et progressiste</strong>.`,
        en: `Designed for daily commute by bike or public transports.<br/><br/>
        4 years of research, design et prototyping. 12 technical innovations free of rights.<br/><br/>
        Hand made to measure in Nice, France, from high quality materials made in France.<br/><br/>
        From design methodology to the opening system through the economic model, all is thought to give the Clin a sane heart, capable to carry your charge and a solid message.`
    },
    //Efficient adj. qui produit un maximum de résultats avec un minimum d’efforts.
    title_efficient: {
        fr: `Accès rapide et sécurisé, ergonomie affûtée`,
        en: `Quick and secure access, sharp ergonomy`
    },
    p_efficient: {
        fr: `Le Clin est avant tout un <strong>système d'ouverture inédit</strong> sans zip ni scratch ni aimant. Il faut acquérir le bon geste pour  ouvrir un Clin, et peut-être même une certaine dextérité. Fastidieux ? - pas au quotidien. A ma connaissance il n'existe aucun autre sac à dos que le Clin qui puisse s'ouvrir <strong>d'une seule main</strong> en 1 seconde et demie... et seulement par son porteur.<br/><br/>
        <strong>Les justées</strong>, le système de réglage des suspentes du Clin, sont fabriquées à partir de chaînes de vélo récupérées et offrent une ergonomie inédite. Le <strong>réglage instantané et intuitif</strong> des justées transforme l'utilisation du sac à dos : en une fraction de seconde, desserrez complètement une bretelle pour mettre ou enlever le Clin. En une fraction de seconde, ajustez le Clin au plus près avant un sprint ou avant d'enfourcher un vélo.<br/><br/>
        Le volume extérieur du Clin s'adapte automatiquement à son contenu, organisé dans <strong>6 poches à usages spécifiques</strong> : un "vide-poche" en matière ultra résistante pour les pièces et les clefs, une poche protégée en laine et satin pour accueillir un ordinateur portable ou une tablette, une poche en satin pour accueillir un smartphone, une poche pour porte-document, la grande poche au <strong>volume flexible</strong> et une poche ...pour le reste.<br/>`,
        en: `The Clin is a all new opening system, without zip nor velcro nor magnet. It takes the good gesture to master it. And then, it is probably the most efficient opening system you can put you hand on: access your content in a snap with only one hand.<br/><br/>
        The "justed", Clin's tuning system, are made of reused bike chains and offer an outstanding ergonomy. The instant and intuitive tuning of justeds transform the hole backpack experience: in a fraction of a second, loosen completely a strap to put on or off the Clin. In a fraction of a second, tighten your Clin before a sprint or before riding a bike.<br/>`
    },
    //Robuste adj. capable de résister à des efforts extrêmes et à un usage prolongé
    title_robuste: {
        fr: `Conçu pour résister et protéger`,
        en: `Designed to resist and protect`
    },
    p_robuste: {
        fr: `<strong>Les suspentes</strong> en Dyneema du Clin supportent <strong>195kg</strong> pour 1mm de diamètre et possèdent surtout une résistance à l'abrasion et une durabilité hors du commun.<br/><br/>
        L'<strong>enveloppe</strong> du Clin est confectionnée dans une bâche en lin tissée à l'origine pour un usage militaire. Elle est <strong>ultra résistante</strong> et son tissage est si dense qu'elle est pratiquement étanche.<br/><br/>
        La laine cardée remplace ici les habituelles mousses synthétiques pour une <strong>durabilité</strong> accrue.<br/>
        Les boucles et rivets du Clin sont tous en acier inoxydable.<br/><br/>
        En plus des matériaux choisis, c'est la <strong>qualité des assemblages</strong> du Clin et la conception de sa structure qui font sa robustesse.<br/><br/>
        Le Clin est conçu pour vous épauler au quotidien et protéger vos biens pour longtemps.<br/>`,
        en: `under construction`
    },
    //Urbain adj. litt. Qui fait preuve de savoir vivre, de politesse, qui agit avec raffinement.
    title_urbain: {
        fr: `Confortable et respectable`,
        en: `Comfortable and respectable`
    },
    p_urbain: {
        fr: `Le Clin introduit l’<strong>épaulière</strong>. Une surface de contact avec vos épaules la plus large possible, sans pli ni couture, pour <strong>répartir parfaitement la pression</strong> jusqu’à la rendre imperceptible ; découpée dans le seul matériau aussi <strong>élastique et respirant</strong> que la peau : … la peau.<br/><br/>
        Aussi discrètes que résistantes, les <strong>suspentes</strong> du Clin transmettent la charge sans gêner vos mouvements. Elles sont ancrées de telle façon qu’elle ne vous touchent pas et offrent une telle <strong>stabilité</strong> que la ceinture abdominale et ses frottements parasites ont disparu.<br/><br/>
        Le Clin sait se tenir. Son <strong>armature en osier</strong> est une combinaison rêvée d’élasticité, de résilience et d’<strong>amortissement des chocs</strong>. L’architecture du Clin maintien en permanence son contenu au plus près de votre dos et le <strong>support lombaire</strong> en laine cardée et osier assure un contact <strong>confortable</strong>, franc et aéré.<br/><br/>
        Le Clin est rempli de <strong>bienveillance</strong>. Il est <strong>fabriqué en France</strong>, avec des matériaux <strong>renouvelables</strong> (Tissus de France, osier de France, laine de France, cuir tannage végétal de Belgique), matériaux <strong>réutilisés</strong> (chaînes de vélo) ou à très grande <strong>durabilité</strong> (Inox de France, Dyneema de France). Le cuir n'est utilisé que là où ses caractéristiques physiques sont irremplaçables. J'ai fabriqué la plupart de mes outils à partir de matériel récupéré.<br/>`,
        en: 'under construction'
    },
    title_demarche: {
        fr: `Démarche`,
        en: `Philosophy`
    },
    p_demarche: {
        fr: `Mes motivations principales sont le plaisir de <strong>créer librement</strong>, un quotidien équilibré entre travail manuel et intellectuel, l'exercice d'un métier sain et la contribution au renouveau des modes de production et de consommation pour un humain en harmonie avec sa planète.<br/><br/>
        En choisissant l'entreprise individuelle, je souhaite établir mon outil de travail dans un principe d'<strong>économie coopérative</strong> et privilégier les relations de <strong>partenariat</strong> plutôt que les rapports de subordination. Je rêve que la réputation d’excellence du Clin m’amène à collaborer avec d’autres artisans français pour développer d’autres objets du quotidien <strong>innovants, durables et produits en France</strong>.<br/><br/>
        La <strong>relocalisation de l'artisanat</strong> et de la petite industrie est un sujet qui me passionne depuis mes études à l’école nationale d’Arts et Métiers. C’est à mon ambition d'y participer que je dois ma persévérance. Les clés du succès dont je souhaite faire la promotion sont l’artisanat, l'économie coopérative, l'économie circulaire, et des principes de conception et de production innovants, comme la <strong>Permafacture</strong>.<br/><br/>
        Au fil de mes expériences, la santé de notre planète est venue se loger au cœur de ma vocation d'ingénieur. Durant la genèse du Clin, l'<strong>écoconception</strong> a cessé d'être une contrainte à intégrer à mes inventions pour devenir la méthodologie par laquelle je crée. Les 12 principes de permaculture selon David Holmgren ont souvent orienté mes décisions de conception et je souhaite vivement participer à l’avènement de la <strong>Permafacture</strong>.<br/><br/>
        Les <strong>outils de production</strong> que j'ai créés pour le Clin, notamment ma presse à plisser, sont essentiellement fabriqués à partir de <strong>matériaux et machines récupérés</strong>. J’ai ainsi, pour un investissement minimal, un outil de production <strong>exclusif, performant et adapté</strong> à mon produit. En outre, les transformations intervenant dans la confection du Clin n’impliquent aucun outil ou produit dangereux et sont conçues pour ne générer ni déchet, ni poussière, ni émanation gazeuse.<br/><br/>
        Le Clin a été conçu de façon à être <strong>réparable et démontable</strong> facilement afin de pouvoir revaloriser ses composants. Ce point pourrait paraître superflu compte tenu de la robustesse du Clin, mais il m’a permis de revaloriser les nombreux prototypes fabriqués et ainsi atteindre mon objectif de <strong>zéro déchet</strong>.`,
        en: `under construction`
    },
    title_order: {
        fr: `Je veux un Clin`,
        en: `I want a Clin`
    },
    p_order: {
        fr: `Le Clin est vendu 360€. Un acompte de 90€ est demandé pour chaque commande.<br/><br/>
        Afin de faire les derniers réglages sur vos épaules et m'assurer que la prise en main de votre nouvel outil est optimale, je livre toujours le Clin en personne.<br/><br/>
        Il est donc possible de prendre possession de votre Clin à Nice bien-sûr, ou lors du salon Made In France 2019 à Paris Porte de Versaille les 8,9,10 et 11 Novembre.<br/><br/>
        Dans tous les cas, je vous invite à m'appeller pour passer commande.
        Alternativement, entrez ci-dessous votre e-mail ou votre numéro de téléphone et le nom de la grande ville proche de chez vous, et je vous contacterai lorsqu'une livraison dans cette ville sera organisée.<br/><br/>`,
        en: `under construction`
    },
    title_liens: {
        fr: `Liens<br/></br>`,
        en: `Links`
    },
    p_liens: {
         fr: `<a href="https://mifexpo4.s3.amazonaws.com/uploads/detail/dossier_presse/1017/Le_Clin_dossier_presse_MIF_I20.pdf" target="_blank">Dossier de presse sur mifexpo.fr</a><br/><br/>
         <a href="https://youtu.be/u7rjJtEgyx4" target="_blank">Reportage France 3 Côte d'Azur</a><br/></br>`,
         en: `under construction`
    },
    title_contact: {
        fr: `Contact`,
        en: `Contact`
    },
    p_contact: {
        fr: `François Garcin, Artisan Ingénieur<br/><br/>
        SIRET : 839 876 505 00018<br/><br/>
        Courriel : <a href="mailto:contact@francoisgarcin.com">contact@francoisgarcin.com</a><br/><br/>
        Numéro de téléphone : <a href="tel:0659437940">0659437940</a><br/><br/>
        N'hésitez pas à me contacter pour toute demande ou information, ou remplissez le formulaire ci-dessous et je vous recontacte rapidement.`,
        en: `under construction`
    },
    b_send: {
        fr: `Envoyer`,
        en: `Send`
    },
    p_footer: {
        fr: `Le Clin évolu, toujours en mieux. Les photos actuellement sur cette page correspondent à un prototype datant de décembre 2017. En attendant les photos de la nouvelle version, je vous invite à visiter la page Facebook.<br/><br/>
        Le Clin fait l'objet d'un dépôt de modèle international qui établi mes droits en tant que créateur et le protège d'un bête copier-coller. En revanche j’ai fait le choix de ne pas déposer de brevet sur les 12 innovations techniques apportées par le Clin. Je libère ainsi en Europe ma création technique selon les termes de la licence libre « Creative Common, Attribution ». Cela signifie que tout le monde peut réutiliser une ou des solutions techniques du Clin en Europe, s’en inspirer librement, même pour une utilisation commerciale, à la simple condition de mentionner mon travail. J'invite bien sûr toute personne intéressée à me contacter... librement.<br/><br/>
        Aucun cookie n'est collecté sur ce site. Les données collectées par formulaire ne sont jamais associées à votre identité, et ne sont utilisées que par François Garcin dans le cadre de votre demande d'information concernant le Clin.<br/><br/>`,
        en: `under construction`
    },
    p_hebergeur: {
        fr: `Hébergé par <a href="https://www.lautre.net">lautre.net</a>, hébergeur associatif autogéré.<br/><br/>`,
        en: `Host by <a href="lautre.net">lautre.net</a>`
    },
    p_powered: {
        fr: `Réalisé par moi-même et les copains: Kalou et Seb`,
        en: `Powered by myself and friends: Kalou and Seb`
    },

};