// -----------------------------------
// IMPORTS, ELEMENTS AND VARIABLES
// -----------------------------------

const dynamicContent = document.querySelectorAll('[data-content]');
const header = document.querySelector('header');
const menu = header.querySelector('.menu');
const sections = document.querySelectorAll('.section');
const menuLinks = document.querySelectorAll('.menu__link');
const stillImage = document.querySelector('.stick-img');

// -----------------------------------
// FUNCTIONS
// -----------------------------------

const getLanguage = () => navigator.language.slice(0, 2) === 'fr' ? 'fr' : 'fr';

const publishDynamicContent = (language) => {
    dynamicContent.forEach(element => {
        element.innerHTML = textContent[element.dataset.content][language];
    });
};

// const updateStillImage = (anchorsArray) => {
//     let lastSlash = stillImage.src.slice(0, stillImage.src.lastIndexOf('/'));
//     let id = anchorsArray.reduce((last, anchor, id) => last = anchor <= window.scrollY ? Math.max(last, +id) : last, 0);
//     stillImage.src = `${lastSlash}/${id}.jpg`;
// };

const updateScrollAnchors = () => {
    const scrollAnchors = [...sections].map((section, id) => section.offsetTop - (id > 0 ? menu.clientHeight : 0));
    menuLinks.forEach((link, id) => {
        link.addEventListener('click', function(e) {
            // updateStillImage(scrollAnchors);
            window.scroll({
                top: id ? scrollAnchors[id] : 0, 
                left: 0, 
                behavior: 'smooth' 
            });
        });
    });
}

// -----------------------------------
// EVENT LISTENERS
// -----------------------------------

window.addEventListener('resize', function(e) {
    updateScrollAnchors();
});

// window.addEventListener('scroll', function(e) {
//     const scrollAnchors = [...sections].map((section, id) => section.offsetTop - (id > 0 ? menu.clientHeight : 0));
//     updateStillImage(scrollAnchors);
// });

window.addEventListener('load', function() {
    publishDynamicContent(getLanguage());
    updateScrollAnchors();
});